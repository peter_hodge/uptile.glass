<?php

require '/home/peter/mosaic.wip-1.0/php/Mosaic.php';

$loader = new Mosaic\Loader;
$loader->loaderAddPath(dirname(__DIR__).'/Glass', 'Mosaic\\Glass');
$loader->loaderLoadTile('Mosaic\\Glass\\Navigator');

?>
<!DOCTYPE html>
<html>
<head>
<style>
<?php echo $loader->loaderGetAllCSS(); ?>
</style>
</head>
<body>
<?php
$navigator = new Mosaic\Glass\Navigator;
echo $navigator->tileGetHTML();
?>
</body>
</html>
