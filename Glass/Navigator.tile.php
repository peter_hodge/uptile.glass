<?php

namespace Mosaic\Glass;

class Navigator implements \Mosaic\Tile {
    use \Mosaic\Traits\Tile;

    // TODO - what does the navigator look like?

    public function tileGetHTML() {
        $text = "Hello world";

        $inner_html = "<span class=\"__ETBR\">$text<span class=\"__refl\">$text</span></span>";

        return "<nav {$this->tileGetAttrs()}>$inner_html</nav>";
    }
}

